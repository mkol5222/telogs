#!/bin/bash

echo "Waiting for InfluxDB"
until $(curl --output /dev/null --silent --fail http://influxdb:8086/query?q=SHOW+DATABASES); do
    printf '.'
    sleep 5
done
echo "InfluxDB ready"

echo "Recreating DB"
curl -v -X POST http://influxdb:8086/query --data-urlencode "q=DROP DATABASE telogs" 
curl -v -X POST http://influxdb:8086/query --data-urlencode "q=CREATE DATABASE telogs" 
curl -v -Gi http://influxdb:8086/query --data-urlencode "q=SHOW DATABASES" 
echo

cd /code
echo "Processing data"
. process.sh
echo "Importing data"
. save.sh

#  curl --output /dev/null --fail http://admin:admin@localhost:3000/api/datasources
echo "Waiting for Grafana"
until $(curl --output /dev/null --silent --fail http://admin:admin@grafana:3000/api/datasources); do
    printf '.'
    sleep 5
done
echo "InfluxDB ready"

echo "Updating Grafana"
curl -0 -v -X POST http://admin:admin@grafana:3000/api/datasources \
-H 'Content-Type: text/json; charset=utf-8' \
-d @/code/datasources.json

curl -0 -v -X POST http://admin:admin@grafana:3000/api/dashboards/db \
-H 'Content-Type: text/json; charset=utf-8' \
-d @/code/grafana-telogs-dashboard.json

echo "Done"
echo "Visit your Grafana at http://your-dockerhost:3000"
