#!/usr/bin/env python 
# ^\s*(\d\d)(\w\w\w)(\d\d\d\d) (\d\d):(\d\d):(\d\d)\s+([^\s]+)\s+([^\s+])\s+(.*)$

#infile="telogs.txt.gz"
#infile="telogs2.txt.gz"


#import gzip
import re
from datetime import datetime
from dateutil.relativedelta import relativedelta
import time
import sys
#from functional import seq

# mark int with i and quote non integers
def formatval(item):
    try:
        res = "%di" % int(item)
    except:
        res = "\"" + item + "\"" 
    return res

for l in sys.stdin:
    match = re.search("^\s*(\d{1,2})(\w\w\w)(\d\d\d\d) (\d\d):(\d\d):(\d\d)\s+([^\s]+)\s+([^\s+])\s+(.*)$", l)
    if match:
        # timestamp
        d = "%s%s%s %s:%s:%s" % (match.group(1),match.group(2),match.group(3),match.group(4),match.group(5),match.group(6))
        ts = datetime.strptime(d, "%d%b%Y %H:%M:%S")
        timestampint = time.mktime(ts.timetuple())
        # origin
        origin = match.group(7)

        # logfields
        logfields = {}
        # origin
        logfields["origin"] = match.group(7)
        
        logfields_string = match.group(9)
        # split string into fields by ; 
        logfields_list = logfields_string.split(";")

        for logfield in logfields_list:
            l = logfield.split(":")
            k = l[0]
            v = l[1]
            logfields[k] = formatval(v)
            
        # tagfields
        tagfields_list = []
        if logfields["log_id"] == "4005i":
            wanttags= ["origin","log_id","file_type"]
            wantfields = ["scanned","malware_detected","threatcloud_scanned","threatcloud_malware","filter_by_static_analysis","cache_hit_rate","error_count","no_resource_count"]
       
        else:
            wanttags = ["origin","log_id"]
            wantfields = logfields.keys()
            wantfields.remove("origin")
            wantfields.remove("log_id")

        for k in wanttags:
            tagfields_list.append("%s=%s" % (k, logfields[k]))
            del logfields[k]
        tags = ",".join(tagfields_list)
        
        logfields_list = []
        # print logfields

        for k in wantfields:
            logfields_list.append("%s=%s" % (k, logfields[k]))
        fields =  ",".join(logfields_list)  

        print "telog,%s %s %d" % (tags, fields, timestampint) 
        

    